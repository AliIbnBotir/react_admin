import { Outlet, useParams } from 'react-router-dom'
export default function App () {
    const param = useParams()

    return (
        <div>
            <div className="bg-blue-200 w-full">
                Users
            </div>
            <div className={ param.id ? "w-full bg-red-200 justify-center" : "w-full bg-fuchsia-600 justify-center"}>
                <Outlet />
            </div>
        </div>
    )
}